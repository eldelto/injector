defmodule InjectorTest do
  use ExUnit.Case
  doctest Injector

  use Injector

  @test_value "Bob"

  test "fallback value" do
    inject Module.A
    assert "Hello A " <> @test_value == A.hello(@test_value)
  end

  test "injected value" do
    inject Module.B
    assert "Hello A " <> @test_value == B.hello(@test_value)
  end

  test "fallback value with short name" do
    inject Module.A, as: Test
    assert "Hello A " <> @test_value == Test.hello(@test_value)
  end

  test "injected value with short name" do
    inject Module.B, as: Test
    assert "Hello A " <> @test_value == Test.hello(@test_value)
  end

  test "aliased fallback value" do
    alias Module.A, as: Test
    inject Test
    assert "Hello A " <> @test_value == Test.hello(@test_value)
  end

  test "aliased injected value" do
    alias Module.B, as: Test
    inject Test
    assert "Hello A " <> @test_value == Test.hello(@test_value)
  end

  test "aliased fallback value with short name" do
    alias Module.A, as: Test
    inject Test, as: Test1
    assert "Hello A " <> @test_value == Test1.hello(@test_value)
  end

  test "aliased injected value with short name" do
    alias Module.B, as: Test
    inject Test, as: Test1
    assert "Hello A " <> @test_value == Test1.hello(@test_value)
  end
end
