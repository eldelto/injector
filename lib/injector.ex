defmodule Injector do
  defmacro __using__(_) do
    quote do
      import Injector, only: [inject: 1, inject: 2]
    end
  end

  defmacro inject({:__aliases__, _, modules}) do
    module = modules |> modules_to_elixir_module() |> get_module_or_alias(__CALLER__)
    injected_module = injected_module_or_default(module)
    short_name = List.last(modules)
    short_module = modules_to_elixir_module([short_name])

    quote do
      alias unquote(injected_module), as: unquote(short_module)
    end
  end

  defmacro inject({:__aliases__, _, modules}, as: {:__aliases__, _, short_names}) do
    module = modules |> modules_to_elixir_module() |> get_module_or_alias(__CALLER__)
    injected_module = injected_module_or_default(module)
    short_module = modules_to_elixir_module(short_names)

    quote do
      alias unquote(injected_module), as: unquote(short_module)
    end
  end

  defp injected_module_or_default(module) do
    injected_module = Application.get_env(:injector, module)

    if injected_module != nil do
      injected_module
    else
      module
    end
  end

  defp get_modules(function_call) do
    function_call
    |> elem(0)
    |> elem(2)
    |> Enum.at(0)
    |> elem(2)
  end

  defp get_module_or_alias(module, env) do    
    aliases =
      env.aliases
      |> Enum.filter(fn {key, _} -> key == module end)
      |> Enum.map(fn {_, value} -> value end)

    if Enum.empty?(aliases) do
      module
    else
      Enum.at(aliases, 0)
    end
  end

  defp modules_to_string(modules) do
    Enum.join(modules, ".")
  end

  defp string_to_elixir_module(string) do
    String.to_atom("Elixir." <> string)
  end

  defp modules_to_elixir_module(modules) do
    modules |> modules_to_string() |> string_to_elixir_module()
  end
end
